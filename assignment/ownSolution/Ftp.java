import java.io.*;
import java.net.*;
import java.util.*;



public class Ftp {

	boolean debug = false;				// set this to true to print out debug information
	boolean overwriteFile = true;		// if true: overwrite file with same name when downloading
	String _server;						// 
	String _controlport;				// 
	String _dataport;					// 
	int _localdataportdec;				//
	String _localdataport8x8bits; 		//
	String _localdataportbin;			// 

	String _username;					// username/login-name
	String _password;					// password
	String _filesource;
	String _filedest;

	String _downloadDirectory = "./";	// default download path

	DataOutputStream outToServer;		// the bytes we are going to send to the server
	BufferedReader inFromServer;		// the bytes we are going to read from the server


	// constructor
	public Ftp(String source, String dest, String user, String password){
		int tmp;

		// 12345 dec = 11000000111001 bin -> 00110000,00111001 -> 48,57
		_localdataportdec = 12345;
		_localdataport8x8bits = "48,57";
		_controlport = "21";

		tmp = source.toString().indexOf('/');
		_server = parse(source,"/")[0];
		_username = user;
		_password = password;
		_filesource = source.substring(tmp+1);
		_filedest = dest;
		getFile();
	}



	// open connection and get the messages/emails from the server
	void getFile(){
		try{

			String receiveStr;
			InetAddress localhost = InetAddress.getLocalHost();
			int tmp = localhost.toString().indexOf('/');
			String localhostStr = localhost.toString().substring(tmp+1);
			String filename;
			File destFile;

      		debug("localhost: "+localhostStr);

			debug("Receiving file '"+_filesource+"' from '"+_server+":");

			Socket socket = new Socket(_server, Integer.parseInt(_controlport));
			ServerSocket dataSocket = new ServerSocket(_localdataportdec );
			
			outToServer = new DataOutputStream(socket.getOutputStream());
			inFromServer = new BufferedReader( new InputStreamReader(socket.getInputStream()) );

			receiveStr = receive();
			if ( !receiveStr.startsWith("220") ){
				print("Connetion problems: "+receiveStr);
				dataSocket.close();
				socket.close();
				return;
			}

			send("user "+_username);
			receiveStr = receive();
			if ( !receiveStr.startsWith("331") ){
				print("Username not accepted: "+receiveStr);
				dataSocket.close();
				socket.close();
				return;
			}

			send("pass "+_password);
			receiveStr = receive();
			if ( !receiveStr.startsWith("230") ){
				print("Password not accepted: "+receiveStr);
				dataSocket.close();
				socket.close();
				return;
			}


			localhostStr = localhostStr.replace('.',',');
			send("port "+localhostStr+","+_localdataport8x8bits);
			receiveStr = receive();
			if ( !receiveStr.startsWith("200") ){
				print("port command returned error: "+receiveStr);
				dataSocket.close();
				socket.close();
				return;
			}


			send("type I");	// Image type - binary mode
			receiveStr = receive();
			if ( !receiveStr.startsWith("200") ){
				print("Error changing to binary mode: "+receiveStr);
				dataSocket.close();
				socket.close();
				return;
			}


			if (debug){
				send("stat");
				receiveMultiple();
			}

			send("retr "+_filesource);
			receiveStr = receive();
			if ( !receiveStr.startsWith("150") ){
				print("retr command returned error: "+receiveStr);
				debug("This might be caused by a firewall on the client, check settings.");
				dataSocket.close();
				socket.close();
				return;
			}
			
			
			// open data connection
			Socket connectionSocket = dataSocket.accept();
			DataInputStream inFromClient = new DataInputStream(connectionSocket.getInputStream());

			// get the filename from source path
			tmp = _filesource.lastIndexOf('/');
			if (tmp==-1)
				filename = _filesource;
			else{
				filename = _filesource.substring(tmp+1);
			}

			// not empty and contains a slash ('/') or only an "." or ".."
			if ( !_filedest.equals("") && 
					( 	_filedest.indexOf("/")>-1 || 
						(_filedest.indexOf(".")>-1 && _filedest.length()==1) || 
						(_filedest.indexOf("..")>-1 && _filedest.length()==2) ) 
					)
			{
				if ( !_filedest.endsWith("/") )
					_filedest = _filedest+"/";
				filename = _filedest+""+filename;
			// else if _filedest is a new filename
			} else if ( !_filedest.equals("")   ) {
				filename = _filedest;			
			}
			
			filename = _downloadDirectory+filename;

			destFile = new File(filename);
			if ( destFile.exists() && !overwriteFile ){
				print("File exists on local machine: "+filename);
				send("quit");
				socket.close();
				connectionSocket.close();
				dataSocket.close();
				return;
			} else {
				destFile.createNewFile();
			}


			// write inputstream to file
			FileOutputStream fileOutputStream = new FileOutputStream(destFile);
			byte b=0;
			boolean go = true;
			// debug("write data to file: '"+filename+"'");
			if (debug)
				System.out.print("DEBUG: <<Downloading: ");
			while (go) {
				try {
					b = inFromClient.readByte();
				} catch (EOFException e) {
					go = false;
				}

				if (go)
           			fileOutputStream.write(b);
           		if (debug && go)
           			System.out.print("*");
           	}
			if (debug)
				System.out.print("\n");

			fileOutputStream.flush(); 
			fileOutputStream.close(); 
			
			receiveStr = receive();
			if ( !receiveStr.startsWith("226") ){
				print("Transfer problems: "+receiveStr);
				socket.close();
				connectionSocket.close();
				dataSocket.close();
				return;
			}

			send("quit");
			receiveStr = receive();
			if ( !receiveStr.startsWith("221") ){
				print("Problems with closing connection: "+receiveStr);
				socket.close();
				connectionSocket.close();
				dataSocket.close();
				return;
			}

			socket.close();
			connectionSocket.close();
			dataSocket.close();

			//print("File downloaded and written to '"+filename+"'");				// COMMENTED TO SATISFY 9331 TESTING
			print("Download complete.");											// ADDED TO SATISFY 9331 TESTING

		} catch (UnknownHostException e){
			print("Unkown host: "+_server);
		} catch (BindException e){
			print("Socket binding problem");
		} catch (IOException e) {
			print("Invalid destination path");
		} catch (Exception e) {		
			e.printStackTrace();
		}



	}




	// send a command to the server
    void send (String cmdline) {
		try {
			debug(">>" + cmdline);
			outToServer.writeBytes(cmdline + "\r\n");
		} catch (Exception e) {
			e.printStackTrace();	
		}
    }




	// receive a line from server
    String receive() {
		try {
			String line = "";
			line = inFromServer.readLine();
			debug("<<" + line);

			return line;

		} catch (Exception e) {
			e.printStackTrace();	
		}
		return null;
    }






	// receive multiple lines
    void receiveMultiple() {
		try {
			String line = "";
		    boolean finished = false;

		    while (!finished) {
				line = inFromServer.readLine();
				if (line.trim().startsWith("211 End"))
					finished = true;

				debug("<<"+line);
			}

		} catch (Exception e) {
			e.printStackTrace();	
		}
    }





    String[] parse(String str, String delim){
		String[] tokens = null;
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
		tokens = new String[100];

	    while (st.hasMoreTokens()) {
			tokens[i] = new String(st.nextToken());
			i++;
	    }

		return tokens;
    }





	void debug (String line){
		if (debug)
			System.out.println("DEBUG: "+line);
	}



	void print (String line){
			System.out.println(line);
	}


}

