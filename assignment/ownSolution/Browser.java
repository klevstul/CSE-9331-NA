import java.io.*;
import java.util.*;


class Browser {

	static String _statistics;				// global variable to store statistic of last operation


	public static void main(String argv[]) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String commandLine 	= "";

//		System.out.println("kBrowser v0.1 beta\nType 'exit' to exit.");		// LINE COMMENTED FOR 9331 TESTING

		try {
			commandLine = br.readLine();
			while (commandLine!=null){
//				System.out.print("kBrowser> "); 			// LINE COMMENTED FOR 9331 TESTING
				System.out.print("> ");						// LINE ADDED FOR 9331 TESTING
				getInputFromUser(commandLine);
				commandLine = br.readLine();
			}
		} catch (IOException ioe) { 
			System.out.println("IO error!"); 
			System.exit(1); 
		} catch (NullPointerException e){
			System.exit(1);
		}
	}




	static void getInputFromUser(String commandLine){
		String command		= "";

		if (commandLine.equals("")){
			commandLine = "���";
		} else if (commandLine.equals("exit")||commandLine.equals("quit")||commandLine.equals("q")){
			System.exit(1);
		}
		
		command = parse(commandLine," ")[0];

		if (command.equals("download")){
			String url;

			if (noTokens(commandLine," ")!=2){
				usage(11);
				return;
			}

			url = parse(commandLine," ")[1];

			Http http = new Http(url);
			_statistics = http.getStat();
		} else if (command.equals("secure_download")){
			String url;
			String username;
			String password;

			if (noTokens(commandLine," ")!=4){
				usage(12);
				return;
			}

			url 		= parse(commandLine," ")[1];
			username 	= parse(commandLine," ")[2];
			password 	= parse(commandLine," ")[3];

			Http https = new Http(url, username, password);
			_statistics = https.getStat();
		} else if (command.equals("proxy_download")){
			String url;
			String proxyServer;
			String proxyPort;
			String proxyUsername;
			String proxyPassword;

			if (noTokens(commandLine," ")!=6){
				usage(13);
				return;
			}

			url 			= parse(commandLine," ")[1];
			proxyServer 	= parse(commandLine," ")[2];
			proxyPort 		= parse(commandLine," ")[3];
			proxyUsername 	= parse(commandLine," ")[4];
			proxyPassword 	= parse(commandLine," ")[5];

			Http httpp = new Http(url, proxyServer, proxyPort, proxyUsername, proxyPassword);
			_statistics = httpp.getStat();
		} else if (command.equals("secure_proxy_download")){
			String url;
			String proxyServer;
			String proxyPort;
			String proxyUsername;
			String proxyPassword;
			String username;
			String password;

			if (noTokens(commandLine," ")!=8){
				usage(14);
				return;
			}

			url 			= parse(commandLine," ")[1];
			proxyServer 	= parse(commandLine," ")[2];
			proxyPort 		= parse(commandLine," ")[3];
			proxyUsername 	= parse(commandLine," ")[4];
			proxyPassword 	= parse(commandLine," ")[5];
			username 		= parse(commandLine," ")[6];
			password 		= parse(commandLine," ")[7];

			Http httpsp = new Http(url, proxyServer, proxyPort, proxyUsername, proxyPassword, username, password);
			_statistics = httpsp.getStat();
		} else if (command.equals("statistics") || command.equals("stat")){
			if (_statistics == null)
				System.out.println("No statistics to display");
			else
				System.out.print(_statistics);
		} else if (command.equals("read_mail")){
			String server;
			String port;
			String user;
			String password;

			if (noTokens(commandLine," ")!=5){
				usage(2);
				return;
			}
			
			server 		= parse(commandLine," ")[1];
			port 		= parse(commandLine," ")[2];
			user 		= parse(commandLine," ")[3];
			password 	= parse(commandLine," ")[4];
			
			Pop3 pop3 = new Pop3(server,port,user,password);
		} else if (command.equals("ftp")){
			String source;
			String dest;
			String user;
			String password;

			if (noTokens(commandLine," ")!=5){
				usage(3);
				return;
			}
			
			source 		= parse(commandLine," ")[1];
			dest 		= parse(commandLine," ")[2];
			user 		= parse(commandLine," ")[3];
			password 	= parse(commandLine," ")[4];

			Ftp ftp = new Ftp(source,dest,user,password);
		} else if (commandLine.equals("help")){
			System.out.println("Supported commands are:\n");
			System.out.println("download\t\t\t- download a URL");
			System.out.println("secure_download\t\t\t- download a password protected URL");
			System.out.println("proxy_download\t\t\t- download a URL through a proxy server");
			System.out.println("secure_proxy_download\t\t- download a password protected URL through a proxy server");
			System.out.println("statistics\t\t\t- show statistics for last HTTP download");
			System.out.println("read_mail\t\t\t- read mail using POP3");
			System.out.println("ftp\t\t\t\t- receive file from a ftp server");
		} else if (!commandLine.equals("���")){
			System.out.println("Unknown command - type \"help\" for more info");
		}

	}


    static String[] parse(String str, String delim){
		String[] tokens = null;
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
		tokens = new String[100];

	    while (st.hasMoreTokens()) {
			tokens[i] = new String(st.nextToken());
			i++;
	    }
		return tokens;
    }



    static int noTokens(String str, String delim){
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
	    while (st.hasMoreTokens()) {
			st.nextToken();
			i++;
	    }
		return i;
    }



	static void usage(int type){
		if (type == 11){
			System.out.println("HTTP Usage: download <url>");
			System.out.println("example: download klevstul.com\n");
		} else if (type == 12){
			System.out.println("HTTP Usage: secure_download <url> <username> <password>");
			System.out.println("example: download securesite.com/private frode 123abc\n");
		} else if (type == 13){
			System.out.println("HTTP Usage: proxy_download <url> <proxy server> <proxy port> <proxy username> <proxy password>");
			System.out.println("example: proxy_download klevstul.com www-proxy.cse.unsw.edu.au 3128 stud123 123abc\n");
		} else if (type == 14){
			System.out.println("HTTP Usage: secure_proxy_download <url> <proxy server> <proxy port> <proxy username> <proxy password> <username> <password>");
			System.out.println("example: secure_proxy_download klevstul.com www-proxy.cse.unsw.edu.au 3128 stud123 123abc unameit 111aaa\n");
		} else if (type == 2) {
			System.out.println("POP3 Usage: read_mail <pop3-host> <port> <username> <password>");
			System.out.println("example: read_mail incoming.imailbox.com 110 fk 123abc\n");
		} else if (type == 3) {
			System.out.println("FTP Usage: ftp <source> <dest> <user> <password>");
			System.out.println("example: ftp ftp.cse.unsw.edu.au/tmp_amd/billw/Wilson-msv3.gif . anonymous abc@abc.com\n");
		}
	}

}